# UUTest

A _trivial_ unit testing _framework_ for modern C++ (C++ 20).  
Created for testing small projects.

Just a little better than using `assert`:

- You get (by default) a summary with the number of tests passed / failed.
- You can choose if after a failing test, the execution of the test suite stops, the program halts, or the next test is run.
- You can have it as _header only_ or simply copy about 80 lines of code into your code.
- Depends on:

  ```cpp
  #include <iostream>
  #include <source_location>
  #include <string_view>
  #include <exception>
  ```

- Colors.
- No macros (but templates: the resulting executable might be bigger than what you expect).

If you want something slightly more fancy, you can use the `UUTest::UTest` from `utest.h` (see below for the additional feature).

## Usage

Creating a test suite:

- Test suites are bundled inside unnamed scopes.
- The argument to the `start()` function is the – optional – `title` of the test suite.
- Each test can have an – optional – message, that will be shown along with the test results.

```cpp
#include <iostream>
#include <source_location>
#include <string_view>
#include <exception>

namespace UUTest {
    // ... copy paste the code from uutest.h
}


int add(const int a, const int b) {
    return a + b;
}

std::string hello() {
    return "hello";
}

int main() {
    {
        auto uu = UUTest::start("Math library");
        uu.test(add(2, 2) == 4);
        uu.test(add(2, 2) == 5, "2 + 2 == 5");
        uu.test(hello() == "hello", "call to hello()");
    }
}
```

Configuration:

- `show_passing = false`: List each passing test.
- `show_summary = true`: Print a summary with the number of passed and failed tests.
- `halt_suite = false`: A failing test stops the execution of therest of the test suite
- `halt = false`: A failing test halts the program.

Simply set those variables for each test suite. As an example, if you don't want to have the summary avec the number of tests you can set:

```cpp
uu.show_summary = false;
```

## `UUTest::UTest`

`UTest` extends `UUTest`. By passing the actual value and the expected one to the `test` function, you will found the actual values in the _failing_ output.

The tests should work for any type that is _comparable_ (or _totally ordered_, depending on the operator) and implements the stream operator `<<`.

Of course, both actual and expected value must be of the same type.

The configuration options are identical to the ones for `UUTest`

```cpp
#include "UUTest/uutest.h"
#include "UUTest/utest.h"

int add(const int a, const int b) {
    return a + b;
}

std::string hello() {
    return "hello";
}

int main() {
    {
        using namespace std::string_literals;

        auto uu = UUTest::with_tvalues("The other tests");
        uu.test(hello(), "hello"s, "hello()");
        uu.test(hello(), "hellow"s, "hello()");
        uu.test(true, true, UUTest::Op::ne);
        uu.test(add(2, 2), 5, UUTest::Op::ne);
        uu.test(add(2, 2), "a", UUTest::Op::ne);
    }
}
```

## Licence

MIT License 2022 by Ale Rimoldi.
