#pragma once
#include <concepts>

namespace UUTest {
    enum struct Op{
        none,
        eq, ne,
        lt, gt,
        le, ge
        // ae, // approximative equal: for floats
        // is_null, not_null
        // is_exception
    };

    // not all operators apply to every test type
    struct UTest: public UUTest {
        template<typename T>
        void display_expecting(T a, T b, Op op) {
            std::clog << "  Expecting: " << b << "\n";
            switch (op) {
                case Op::eq : std::clog << "  But got  : " << a << "\n"; break;
                case Op::ne : std::clog << "  To be   != " << a << "\n"; break;
                case Op::lt : std::clog << "  To be   <  " << a << "\n"; break;
                case Op::gt : std::clog << "  To be   >  " << a << "\n"; break;
                case Op::le : std::clog << "  To be   <= " << a << "\n"; break;
                case Op::ge : std::clog << "  To be   >= " << a << "\n"; break;
            }
        }

        template<typename T, typename U>
        void display_type_mismatch(T a, U b, const std::string_view message, std::source_location location) {
            display_message(false, message);
            display_location(location);
            std::clog << "  Expecting: " << b << "\n"
            << "  But got  : " << a << "\n"
            << "  of a different type" << "\n";
        }
        
        template<typename T>
        void display_invalid_operator(T b, const Op op, const std::string_view message, std::source_location location) {
            display_message(false, message);
            display_location(location);
            std::clog << "  Expecting: " << b << "\n"
            << "but it has a type that does not support the operator." << "\n";
        }

        template<typename T, typename U>
        void test(T a, U b, const std::string_view message = "", const std::source_location location = std::source_location::current()) {
            test(a, b, Op::eq, message, location);
        }

        template<typename T, typename U>
        void test(T a, U b, Op op, const std::string_view message = "", const std::source_location location = std::source_location::current()) {
            if constexpr (std::is_same<T, U>::value) {
                bool valid_op = true;
                bool succeeded = false;
                if constexpr (std::equality_comparable<T>) {
                    switch (op) {
                        case Op::eq : succeeded = a == b; break;
                        case Op::ne : succeeded = a != b; break;
                    }
                } else {
                    valid_op = false;
                }
                if constexpr (std::totally_ordered<T>) {
                    switch (op) {
                        case Op::lt : succeeded = a <  b; break;
                        case Op::gt : succeeded = a >  b; break;
                        case Op::le : succeeded = a <= b; break;
                        case Op::ge : succeeded = a >= b; break;
                    }
                } else {
                    valid_op = false;
                }
                if (valid_op) {
                    if (display(succeeded, message, location) && !succeeded) {
                        display_expecting(a, b, op);
                    }
                } else {
                    display_invalid_operator(a, op, message, location);
                }
            } else {
                display_type_mismatch(a, b, message, location);
                failed += 1;
            }
        }
    };

    UTest with_values(const std::string_view title = "") {
        return UTest{title};
    }
}
