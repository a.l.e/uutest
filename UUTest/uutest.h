#pragma once
#include <iostream>
#include <source_location>
#include <string_view>
#include <exception>

namespace UUTest {
    struct Exception : public std::exception {
    };

    struct UUTest {
        std::string_view title = "";
        int passed = 0;
        int failed = 0;
        bool show_passing = false;
        bool show_summary = true;
        bool halt_suite = false;
        bool halt = false;

        UUTest(std::string_view title = ""): title{title} {}

        ~UUTest() {
            if (show_summary) {
                if (!show_passing && failed == 0) {
                    display_title(title);
                }
                std::clog << "\033[32m" << passed << " test" << (passed > 1 ? "s" : "") << " passed." << "\033[0m" << "\n";
                std::clog << "\033[31m" << failed << " test" << (failed > 1 ? "s" : "") << " failed." << "\033[0m" << "\n";
            }
        }

        void test(bool succeeded, const std::string_view message = "", std::source_location location = std::source_location::current()) {
            display(succeeded, message, location);
        }

    protected:
        void display_title(const std::string_view title) {
            std::clog << "\033[1m" << title << "\033[0m" << "\n";
        }

        void display_location(const std::source_location location) {
            std::clog << " (" << location.file_name() << "::"
            << location.line() << ")\n";
        }

        void display_message(bool succeeded, const std::string_view message) {
            std::clog << (succeeded ? "\033[32m" : "\033[31m") << "● " << "\033[0m"
            << (message.size() > 0 ? message :  "Failing test");
        }

        bool display(bool succeeded, const std::string_view message, const std::source_location location) {
            if (halt_suite && failed > 0) {
                return false;
            }
            if (succeeded) {
                passed += 1;
                if (!show_passing) {
                    return false;
                }
            } else {
                failed += 1;
            }

            if (title.size() > 0 && failed == 1) {
                display_title(title);
            }

            display_message(succeeded, message);
            display_location(location);
            if (halt) {
                throw Exception();
            }
            return true;
        }

    };

    UUTest start(const std::string_view title = "") {
        return UUTest{title};
    }
}
