#include "../UUTest/uutest.h"
#include "../UUTest/utest.h"

int add(const int a, const int b) {
    return a + b;
}

std::string hello() {
    return "hello";
}

int main() {
    // The first test suite uses the _simple_ UUTest.
    {
        auto uu = UUTest::start("My tests");
        uu.show_passing = true;
        uu.test(add(2, 2) == 4);
        uu.test(add(2, 2) == 5, "2 + 2 == 5");
        uu.test(hello() == "hello", "hello");
    }
    The second test suite uses the _more advanced_ UTest.
    {
        using namespace std::string_literals;

        auto uu = UUTest::with_values("The other tests");
        uu.test(hello(), "hello"s, "hello()");
        uu.test(hello(), "hellow"s, "hello()");
        uu.test(true, true, UUTest::Op::ne);
        uu.test(add(2, 2), 5, UUTest::Op::ne);
        uu.test(add(2, 2), "a", UUTest::Op::ne);
    }
}
